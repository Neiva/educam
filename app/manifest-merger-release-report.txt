-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
	package
		ADDED from AndroidManifest.xml:3:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionCode
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-feature#android.hardware.camera
ADDED from AndroidManifest.xml:5:5
	android:required
		ADDED from AndroidManifest.xml:7:9
	android:name
		ADDED from AndroidManifest.xml:6:9
uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:9:5
	android:name
		ADDED from AndroidManifest.xml:9:22
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:10:5
	android:name
		ADDED from AndroidManifest.xml:10:22
uses-permission#android.permission.ACCESS_NETWORK_STATE
ADDED from AndroidManifest.xml:11:5
	android:name
		ADDED from AndroidManifest.xml:11:22
uses-permission#com.google.android.providers.gsf.permission.READ_GSERVICES
ADDED from AndroidManifest.xml:12:5
	android:name
		ADDED from AndroidManifest.xml:12:22
uses-permission#android.permission.ACCESS_COARSE_LOCATION
ADDED from AndroidManifest.xml:13:5
	android:name
		ADDED from AndroidManifest.xml:13:22
uses-permission#android.permission.ACCESS_FINE_LOCATION
ADDED from AndroidManifest.xml:14:5
	android:name
		ADDED from AndroidManifest.xml:14:22
application
ADDED from AndroidManifest.xml:22:5
MERGED from com.android.support:appcompat-v7:21.0.3:16:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.android.support:cardview-v7:21.0.3:16:5
MERGED from com.android.support:recyclerview-v7:21.0.3:17:5
MERGED from com.android.support:support-v4:21.0.3:16:5
MERGED from com.google.android.gms:play-services:6.5.87:20:5
MERGED from com.android.support:support-v4:21.0.3:16:5
	android:label
		ADDED from AndroidManifest.xml:25:9
	android:allowBackup
		ADDED from AndroidManifest.xml:23:9
	android:icon
		ADDED from AndroidManifest.xml:24:9
	android:theme
		ADDED from AndroidManifest.xml:26:9
	android:largeHeap
		ADDED from AndroidManifest.xml:27:9
activity#com.example.caique.educam.Activities.MainActivity
ADDED from AndroidManifest.xml:28:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:32:13
	android:label
		ADDED from AndroidManifest.xml:30:13
	android:theme
		ADDED from AndroidManifest.xml:31:13
	android:name
		ADDED from AndroidManifest.xml:29:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:33:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:34:17
	android:name
		ADDED from AndroidManifest.xml:34:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:36:17
	android:name
		ADDED from AndroidManifest.xml:36:27
meta-data#com.google.android.gms.version
ADDED from AndroidManifest.xml:40:9
MERGED from com.google.android.gms:play-services:6.5.87:21:9
	android:name
		ADDED from AndroidManifest.xml:41:13
	android:value
		ADDED from AndroidManifest.xml:42:13
meta-data#com.google.android.maps.v2.API_KEY
ADDED from AndroidManifest.xml:43:9
	android:name
		ADDED from AndroidManifest.xml:44:13
	android:value
		ADDED from AndroidManifest.xml:45:13
activity#com.example.caique.educam.Activities.MapsActivity
ADDED from AndroidManifest.xml:47:9
	android:label
		ADDED from AndroidManifest.xml:50:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:49:13
	android:name
		ADDED from AndroidManifest.xml:48:13
activity#com.example.caique.educam.Activities.PostActivity
ADDED from AndroidManifest.xml:52:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:55:13
	android:label
		ADDED from AndroidManifest.xml:56:13
	android:parentActivityName
		ADDED from AndroidManifest.xml:54:13
	android:name
		ADDED from AndroidManifest.xml:53:13
activity#com.example.caique.educam.Activities.LoginActivity
ADDED from AndroidManifest.xml:58:9
	android:label
		ADDED from AndroidManifest.xml:60:13
	android:name
		ADDED from AndroidManifest.xml:59:13
activity#com.example.caique.educam.Activities.TimelineActivity
ADDED from AndroidManifest.xml:62:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:64:13
	android:label
		ADDED from AndroidManifest.xml:65:13
	android:name
		ADDED from AndroidManifest.xml:63:13
uses-sdk
INJECTED from AndroidManifest.xml:0:0 reason: use-sdk injection requested
MERGED from com.android.support:appcompat-v7:21.0.3:15:5
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.android.support:cardview-v7:21.0.3:15:5
MERGED from com.android.support:recyclerview-v7:21.0.3:15:5
MERGED from com.android.support:support-v4:21.0.3:15:5
MERGED from com.google.android.gms:play-services:6.5.87:18:5
MERGED from com.android.support:support-v4:21.0.3:15:5
	android:targetSdkVersion
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
activity#android.support.v7.widget.TestActivity
ADDED from com.android.support:recyclerview-v7:21.0.3:18:9
	android:label
		ADDED from com.android.support:recyclerview-v7:21.0.3:18:19
	android:name
		ADDED from com.android.support:recyclerview-v7:21.0.3:18:60
