package com.example.caique.educam.Map;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.caique.educam.R;
import com.example.caique.educam.Tools.RecyclerViewClickListener;

import java.util.List;

/**
 * Created by caique on 27/04/15.
 */
public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder> {
    private Context mContext;
    private static List<Item> mDataSet;
    public static RecyclerViewClickListener mItemListener;

    public CustomAdapter(Context context, List<Item> dataSet, RecyclerViewClickListener itemListener){
        mContext = context;
        mDataSet = dataSet;
        mItemListener = itemListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView mCityName;
        TextView mText;

        public ViewHolder(final View itemView) {
            super(itemView);
            mCityName = (TextView) itemView.findViewById(R.id.city);
            mText = (TextView) itemView.findViewById(R.id.text);

            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    Log.d(getClass().getName(), "Element " + getPosition() + " clicked");
                    mItemListener.recyclerViewListClicked(v, getPosition(), mDataSet.get(getPosition()));
                }
            });
        }

        public TextView getCity(){return mCityName;}
        public TextView getText(){return mText;}
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.location_list_item, parent, false);

        return new ViewHolder(v);
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.d(getClass().getName(), "Element " + position + " set.");
        Item local_item = mDataSet.get(position);

        holder.getCity().setText(local_item.getCity());
        holder.getText().setText(local_item.getText());
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }
}
