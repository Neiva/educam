package com.example.caique.educam.Components;

import com.google.android.gms.maps.model.LatLng;

import java.sql.Timestamp;

/**
 * Created by caique on 09/03/15.
 */
public class Post {
    private int id;
    private int user;
    private String user_name;
    private String photo;
    private String title;
    private double latitude;
    private double longitude;
    private String city;
    private String street;
    private int likes = 0;

    public char getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(char isLiked) {
        this.isLiked = isLiked;
    }

    private int dislikes = 0;
    private char isLiked = 'I';
    private Timestamp created_at;

    public int getDislikes() {
        return dislikes;
    }

    public void setDislikes(int dislikes) {
        this.dislikes = dislikes;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public String getPhoto() {
        return photo;
    }

    public void setLikes(int likes) { this.likes = likes; }

    public int getLikes() {
        return likes;
    }

    public void liked() {
        this.likes = this.likes + 1;
    }

    public void disliked() {
        this.likes = this.likes - 1;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LatLng getLocation() {
        return new LatLng(getLatitude(), getLongitude());
    }

    public void setLocation(double latitude, double longitude) {
        setLatitude(latitude);
        setLongitude(longitude);
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public String toString() {
        return  "Id:" + getId()
                + " User: " + getUserName()
                + " Photo: " + getPhoto()
                + " Likes: " + getLikes()
                + " Title: " + getTitle()
                + " Location: " +getLocation()
                + " Created_at: " +getCreated_at();
    }

    public String getUserName() {
        return user_name;
    }

    public void setUserName(String user_name) {
        this.user_name = user_name;
    }
}
