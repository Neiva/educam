package com.example.caique.educam.Tools;

import android.view.View;

import com.example.caique.educam.Map.Item;

/**
 * Created by caique on 29/04/15.
 */
public interface RecyclerViewClickListener {
    public void recyclerViewListClicked(View v, int position, Item item);
}
