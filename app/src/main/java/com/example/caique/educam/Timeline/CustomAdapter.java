/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package com.example.caique.educam.Timeline;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.caique.educam.Components.Post;
import com.example.caique.educam.R;
import com.example.caique.educam.Tools.Constants;
import com.example.caique.educam.Tools.EducamPreferences;
import com.example.caique.educam.Tools.Request;
import com.example.caique.educam.Tools.Tools;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Provide views to RecyclerView with data from mDataSet.
 */
public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder> {
    private static final String TAG = "CustomAdapter";

    private static Post[] mDataSet;
    private static Context mContext;
    private static boolean TAG_TYPE;
    // BEGIN_INCLUDE(recyclerViewSampleViewHolder)
    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView mUserText;
        private final TextView mSchoolText;
        private final TextView mDateText;
        private final TextView mLocationText;
        private final TextView mLikeText;
        private final TextView mDislikeText;
        private final ImageView mPhotoImage;
        private final ImageView mUserButton;
        private final ImageView mLikeButton;
        private final ImageView mDislikeButton;
        private final ProgressBar mProgress;
        private final ProgressBar mProgressLike;
        private final ProgressBar mProgressDislike;
        AsyncTask<String,Void,Request> mAsyncTask;

        public ViewHolder(View v) {
            super(v);

            mDateText = (TextView) v.findViewById(R.id.date_text);
            mSchoolText = (TextView) v.findViewById(R.id.school_text);
            mUserText = (TextView) v.findViewById(R.id.user_text);
            mLocationText = (TextView) v.findViewById(R.id.location_text);
            mLikeText = (TextView) v.findViewById(R.id.like_text);
            mDislikeText = (TextView) v.findViewById(R.id.dislike_text);
            mPhotoImage = (ImageView) v.findViewById(R.id.photo_image);
            mProgress = (ProgressBar) v.findViewById(R.id.photo_image_progress);
            mProgressLike = (ProgressBar) v.findViewById(R.id.progressBar2);
            mProgressDislike = (ProgressBar) v.findViewById(R.id.progressBar3);
            mUserButton = (ImageView) v.findViewById(R.id.user_button);
            mLikeButton = (ImageView) v.findViewById(R.id.like_button);
            mDislikeButton = (ImageView) v.findViewById(R.id.dislike_button);

            // Define click listener for the ViewHolder's View.
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Element " + getPosition() + " clicked.");
                }
            });

            mLikeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v){
                    Log.e(getClass().getName(), "" + mDataSet[getPosition()].getIsLiked());
                    if(mDataSet[getPosition()].getIsLiked() == 'I') {
                        TAG_TYPE = true;
                        like("yes", String.valueOf(mDataSet[getPosition()].getId()), "LIKE");
                        Log.e(getClass().getName(), "click on like: " + getPosition());
                    }
                }
            });

            mDislikeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v){
                    Log.e(getClass().getName(), "" + mDataSet[getPosition()].getIsLiked());
                    if(mDataSet[getPosition()].getIsLiked() == 'I') {
                        TAG_TYPE = false;
                        like("no", String.valueOf(mDataSet[getPosition()].getId()), "DISLIKE");
                        Log.e(getClass().getName(), "click on dislike: " + getPosition());
                    }
                }
            });

            mLocationText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    double latitude = mDataSet[getPosition()].getLatitude();
                    double longitude = mDataSet[getPosition()].getLongitude();
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                    browserIntent.setData(Uri.parse(Constants.MAP_BASIS_URL_2
                            + latitude + ","
                            + longitude
                            + "?z=18&?q="
                            + latitude + ","
                            + longitude + "(Aqui!)"));
                    browserIntent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    browserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(browserIntent);
                    Log.e(getClass().getName(), "click on location: " + getPosition());
                }
            });

        }

        public TextView getDateText() { return mDateText; }
        public TextView getSchoolText() { return mSchoolText; }
        public TextView getUserText() { return mUserText; }
        public TextView getLocationText() { return mLocationText; }
        public TextView getLikeText() { return mLikeText; }
        public TextView getDislikeText() { return mDislikeText; }
        public ImageView getPhotoImage() { return mPhotoImage; }
        public ImageView getDislikeButton() { return mDislikeButton; }
        public ImageView getLikeButton() { return mLikeButton; }
        public ProgressBar getProgress() { return mProgress; }

        private void like(String isEduc, String postId, String type){
            mAsyncTask = new AsyncTask<String, Void, Request>() {
                private JSONObject rawJson = new JSONObject();
                private Request checkReq = new Request();

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    if ( TAG_TYPE ) {
                        mLikeButton.setVisibility(View.GONE);
                        mProgressLike.setVisibility(View.VISIBLE);
                    } else {
                        mDislikeButton.setVisibility(View.GONE);
                        mProgressDislike.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                protected Request doInBackground(String... strings) {
                    try {
                        rawJson.put("isEduc", strings[0]);
                        rawJson.put("post", strings[1]);
                        rawJson.put("type", strings[2]);
                        rawJson.put("user", EducamPreferences.getUserId(mContext));
                        Log.e(getClass().getName(), "I'm the rawJSON: " + rawJson.toString());
                        checkReq.setType(strings[2]);
                        checkReq.setResponse(Tools.POST(mContext, rawJson, "post/like"));
                        return checkReq;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Request request) {
                    if ( TAG_TYPE ) {
                        mProgressLike.setVisibility(View.GONE);
                        mLikeButton.setVisibility(View.VISIBLE);
                    } else {
                        mProgressDislike.setVisibility(View.GONE);
                        mDislikeButton.setVisibility(View.VISIBLE);
                    }
                    try {
                        handleConnection(request);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            };

            mAsyncTask.execute(isEduc, postId, type);
        }

        public void handleConnection(Request request) throws JSONException {
            JSONObject response;
            response = request.getResponse();
            String likes;

            switch (request.getType()) {
                case "LIKE":
                    if(response.getString("status").equals("success") && response != null) {
                        getLikeButton().setBackgroundResource(R.drawable.thumbsupgray);
                        mDataSet[getPosition()].setLikes(mDataSet[getPosition()].getLikes() + 1);
                        mDataSet[getPosition()].setIsLiked('L');
                        getLikeText().setText( mDataSet[getPosition()].getLikes() + "" );
                    } else {
                        Toast.makeText(mContext,"POR FAVOR TENTE MAIS TARDE",Toast.LENGTH_SHORT).show();
                    }
                    break;
                case "DISLIKE":
                    if(response.getString("status").equals("success") && response != null) {
                        getDislikeButton().setBackgroundResource(R.drawable.thumbsdowngray);
                        mDataSet[getPosition()].setDislikes(mDataSet[getPosition()].getDislikes() + 1);
                        getDislikeText().setText(mDataSet[getPosition()].getDislikes() + "");
                        mDataSet[getPosition()].setIsLiked('D');
                    } else {
                        Toast.makeText(mContext,"POR FAVOR TENTE MAIS TARDE",Toast.LENGTH_SHORT).show();
                    }
                    break;
                default:
                    Toast.makeText(mContext,"POR FAVOR TENTE MAIS TARDE",Toast.LENGTH_SHORT).show();
            }
        }
    }
    // END_INCLUDE(recyclerViewSampleViewHolder)

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used by RecyclerView.
     */
    public CustomAdapter(Context context, Post[] dataSet) {
        mDataSet = dataSet;
        mContext = context;
    }

    // BEGIN_INCLUDE(recyclerViewOnCreateViewHolder)
    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.post_item, viewGroup, false);

        return new ViewHolder(v);
    }
    // END_INCLUDE(recyclerViewOnCreateViewHolder)

    // BEGIN_INCLUDE(recyclerViewOnBindViewHolder)
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");
        Post local_post = mDataSet[position];

        // Get element from your dataset at this position and replace the contents of the view
        // with that element
        viewHolder.getSchoolText().setText(local_post.getTitle());
        viewHolder.getDateText().setText(local_post.getCreated_at().toString());
        viewHolder.getUserText().setText("Usuário " + local_post.getUser());
        viewHolder.getLocationText().setText(local_post.getLocation().toString());
        viewHolder.getLikeText().setText("" + local_post.getLikes());
        viewHolder.getDislikeText().setText("" + local_post.getDislikes());
        setLikeView(viewHolder.getLikeButton(), viewHolder.getDislikeButton(), local_post.getIsLiked());
        //Tools.setPicUrl(local_post.getPhoto(), viewHolder.getPhotoImage(), viewHolder.getProgress());
        Picasso.with(mContext).load(Constants.MAIN_URL + "uploads/" + local_post.getId() + ".jpg")
                .resize(Resources.getSystem().getDisplayMetrics().widthPixels,Resources.getSystem().getDisplayMetrics().widthPixels)
                .centerCrop().into(viewHolder.getPhotoImage());

    }

    private void setLikeView(ImageView likeButton, ImageView dislikeButton, char isLiked) {
        switch (isLiked) {
            case 'L':
                likeButton.setBackgroundResource(R.drawable.thumbsupgray);
                break;
            case 'D':
                dislikeButton.setBackgroundResource(R.drawable.thumbsdowngray);
        }
    }
    // END_INCLUDE(recyclerViewOnBindViewHolder)

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataSet.length;
    }
}
