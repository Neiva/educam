package com.example.caique.educam.Tools;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Scanner;

/**
 * Created by caique on 15/03/15.
 */
public final class Tools {
    private static AsyncTask<String, Void, Bitmap> retreiveImage;
    private static Bitmap storedBitmap = null;
    public static boolean TAG_THERE_IS_THREAD;
    static Bitmap mLoadedBitmap;

    public static void setPic(String photoPath, ImageView imageView) {
        // Get the dimensions of the View
        int targetW = imageView.getWidth();
        int targetH = imageView.getHeight();

        if(targetW == 0 || targetH == 0)
        {
            targetW = 1080;
            targetH = 661;
        }

        Log.e("on toools", "width" + targetW + "Height" + targetH);

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(photoPath, bmOptions);
        imageView.setImageBitmap(bitmap);
    }

    public static void setPicUrl(final String urlString,
                                 final ImageView imageView,
                                 final ProgressBar progressBar){

        new Thread(new Runnable() {
            @Override
            public void run() {
                downloadImage(urlString, progressBar, imageView);
                }
            }).start();
    }

    public static void downloadImage(String url, final ProgressBar progressBar, final ImageView imageView){

            retreiveImage = new AsyncTask<String, Void, Bitmap>() {

            protected void onPreExecute() {
                super.onPreExecute();
                TAG_THERE_IS_THREAD = true;
            }

            @Override
            protected Bitmap doInBackground(String... urls) {
                return loadImagemFromNetwork(urls[0]);
            }

            @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);

                //final Bitmap oldBitmap = mLoadedBitmap;
                mLoadedBitmap = bitmap;
                progressBar.setVisibility(View.GONE);
                imageView.setImageBitmap(mLoadedBitmap);
                imageView.setVisibility(View.VISIBLE);

                //cleaning previous bitmaps (android doesnt do it by itself)
               /* if(oldBitmap != null && !oldBitmap.isRecycled()) {
                    oldBitmap.recycle();
                    Log.e(getClass().getName(), "recycling a bitmap");
                }*/

                //problem is, this reference is multiplied by the numb of threads
                //which causes to collapse when trying to recyle something already recycled
            }

        };

        retreiveImage.execute(url);
    }

    private static Bitmap loadImagemFromNetwork(String urlString){
        try {
            URL url = new URL(urlString);
            return BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //use this method later to calculate how much shorten a bitmap
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static JSONObject POST(Context context, JSONObject rawJson, String UrlEnd) throws JSONException {
        URL url;
        HttpURLConnection connection;

        try{
            url = new URL(Constants.MAIN_URL + UrlEnd);
            String param="json=" + URLEncoder.encode(rawJson.toString(), "UTF-8");
            //verificar excecao todo
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setFixedLengthStreamingMode(param.getBytes().length);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            PrintWriter out = new PrintWriter(connection.getOutputStream());
            out.print(param);
            out.close();
            String response = "";
            Scanner inStream = new Scanner(connection.getInputStream());
            while (inStream.hasNextLine()){
                response += (inStream.nextLine());
            }
            Log.e("Response Stream: ", response);
            return new JSONObject(response);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
