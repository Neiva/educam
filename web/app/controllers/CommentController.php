<?php

class CommentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /post
	 *
	 * @return Response
	 */
	public function getIndex()
	{
	   return Response::make('You can try /comment or /edit or /delete');	
	}
  
  public function anyComment() {
    $json = json_decode(Input::get("json"));
    
    try {
      $comment = new Comment;
      $comment->user = $json->user;
      $comment->post = $json->post;
      $comment->comment = $json->comment;
      $comment->save();
      $result = ["status"=>"success"];
    } catch ( Exception $e ){
      $result = ["status"=>"fail"];
    }
    
    return Response::json($result);    
  }
  
  public function anyEdit() {
    $json = json_decode(Input::get("json"));
    
    try {
      $comment = Comment::find($json->comment);
      $comment->user = $json->user;
      $comment->post = $json->post;
      $comment->comment = $json->comment;
      $comment->save();
      $result = ["status"=>"success"];
    } catch (Exception $e) {
      $result = ["status"=>"fail"];
    }
    
    return Response::json($result);
  }
  
  public function anyDelete() {
    $json = json_decode(Input::get("json"));
    
    try {
      $comment = Comment::find($json->comment);
      $comment->delete();
      $result = ["status"=>"success"];
    } catch (Exception $e) {
      $result = ["status"=>"fail"];
    }
    
    return Response::json($result);
  }
  
  public function anyRetreive() {
    $json = json_decode(Input::get("json"));
    
    try {
      $comment = Comment::wherePost($json->post)->all();
      $result = ["status"=>"success", "comments"=>$comment];
    } catch (Exception $e) {
      $result = ["status"=>"fail"];
    }
    
    return Response::json($result);
  }
}